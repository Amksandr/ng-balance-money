import { Component } from '@angular/core';

@Component({
    selector: 'bm-loader',
    template: `<div class="loader-animator"></div>`,
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {}
