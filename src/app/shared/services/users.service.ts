import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseApi } from '../core/base-api';

@Injectable({ providedIn: 'root' })
export class UsersService extends BaseApi {
    constructor(public http: HttpClient) {
        super(http);
    }

    getUserByEmail(email: string): Observable<User> {
        // return this.http.get<User>(`http://localhost:3000/users?email=${email}`)
        // .pipe( map((user: User[]) => user[0] ? user[0] : undefined));
        return this.get(`users?email=${email}`).pipe(
            map((users: User[]) => users[0] ? users[0] : undefined )
        );
    }

    createNewUser(user: User): Observable<User> {
        // this.http.post<User>('http://localhost:3000/users', user);
        // .pipe( map((user: User[]) => user) );

        return this.post('users', user);

    }

}
