import { Component, OnInit } from '@angular/core';
import { Category } from '../shared/models/categoty.model';
import { CategoriesService } from '../shared/services/categories.service';

@Component({
  selector: 'bm-records-page',
  templateUrl: './records-page.component.html',
  styleUrls: ['./records-page.component.scss']
})
export class RecordsPageComponent implements OnInit {

  categories: Category[] = [];
  isLoaded = false;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.categoriesService.getCategories()
      .subscribe((categories: Category[]) => {
        this.categories = categories;
        this.isLoaded = true;
      });
  }

  newCategoryAdded(category: Category) {
    console.log(category);
    this.categories.push(category);
  }

  categoryWasEdited(category: Category) {
    const idx = this.categories
      .findIndex( c => c.id === category.id);
    this.categories[idx] = category;
  }

}
