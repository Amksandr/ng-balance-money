import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CategoriesService } from '../../shared/services/categories.service';
import { Category } from '../../shared/models/categoty.model';
import { Subscription } from 'rxjs';


@Component({
  selector: 'bm-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit, OnDestroy {

  @Output() categoryAdd = new EventEmitter<Category>();

  sub1: Subscription;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.sub1) { this.sub1.unsubscribe(); }
  }

  onSubmit(form: NgForm) {
    console.log(form);
    let { name, capacity } = form.value;

    if (capacity < 0) {
      capacity *= -1;
    }

    const category = new Category(name, capacity);

    this.sub1 = this.categoriesService.addCategory(category)
      .subscribe((getCategory: Category) => {
        console.log('[cat]', getCategory);

        form.reset();
        form.form.patchValue({capacity: 1});

        this.categoryAdd.emit(category);

      });


  }

}
