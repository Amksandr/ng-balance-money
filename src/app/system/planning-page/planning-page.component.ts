import { Component, OnInit, OnDestroy } from '@angular/core';
import { BillService } from '../shared/services/bill.service';
import { CategoriesService } from '../shared/services/categories.service';
import { EventsService } from '../shared/services/events.service';
import { combineLatest, Subscription } from 'rxjs';
import { Bill } from '../shared/models/bill.model';
import { BMEvent } from '../shared/models/event.model';
import { Category } from '../shared/models/categoty.model';

@Component({
  selector: 'bm-planning-page',
  templateUrl: './planning-page.component.html',
  styleUrls: ['./planning-page.component.scss']
})
export class PlanningPageComponent implements OnInit, OnDestroy {

  s1: Subscription;
  isLoaded = false;
  bill: Bill;
  categories: Category[] = [];
  events: BMEvent[] = [];

  constructor(
    private billService: BillService,
    private categoriesService: CategoriesService,
    private eventsService: EventsService
  ) { }

  ngOnInit() {
    this.s1 = combineLatest(
      this.billService.getBill(),
      this.categoriesService.getCategories(),
      this.eventsService.getEvents()
    ).subscribe((data: [Bill, Category[], BMEvent[]]) => {
      this.bill = data[0];
      this.categories = data[1];
      this.events = data[2];
      this.isLoaded = true;
    });
  }

  ngOnDestroy() {
    if (this.s1) { this.s1.unsubscribe(); }
  }

  getCategoryCost(cat: Category) {
    const catEvents = this.events.filter((e) => {
      return e.category === cat.id && e.type === 'outcome';
    });

    return catEvents.reduce((total, e) => {
      total += e.amount;
      return total;
    }, 0);
  }

  private getPerscent(cat: Category): number {
    const percent = (100 * this.getCategoryCost(cat)) / cat.capacity;
    return percent > 100 ? 100 : percent;
  }

  getCatPercent(cat: Category): string {
    return this.getPerscent(cat) + '%';
  }

  getCatColorClass(cat: Category): string {
    const percent = this.getPerscent(cat);
    const className = percent < 60 ? 'success' : percent >= 100 ? 'danger' : 'warning';
    return className;
  }

}
