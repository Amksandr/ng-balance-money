import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { Bill } from '../models/bill.model';
import { BaseApi } from '../../../shared/core/base-api';

@Injectable()
export class BillService extends BaseApi {

    constructor(public http: HttpClient) {
        super(http);
    }

    getBill(): Observable<Bill> {
        // this.http.get<Bill>('http://localhost:3000/bill');
        return this.get('bill');
    }

    updateBill(bill: Bill): Observable<Bill> {
        return this.put('bill', bill);
    }

    getCurrency(base: string = 'RUB'): Observable<any> {
        // return this.http.get(`http://localhost:3000/carrency?base=${base}`);
        return this.get(`carrency?base=${base}`);
    }
}
