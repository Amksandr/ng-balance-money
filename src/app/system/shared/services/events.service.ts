import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseApi } from 'src/app/shared/core/base-api';
import { Observable } from 'rxjs';
import { BMEvent } from '../models/event.model';

@Injectable()
export class EventsService extends BaseApi {
    constructor(public http: HttpClient) {
        super(http);
    }

    addEvent(event: BMEvent): Observable<BMEvent> {
        return this.post('events', event);
    }

    getEvents(): Observable<BMEvent[]> {
        return this.get('events');
    }

    getEventById(id: string): Observable<BMEvent> {
        return this.get(`events/${id}`);
    }
}
