import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'bmFilter'
})
export class FilterPipe implements PipeTransform {
    transform(items: any, value: string, field: string): any {
        if (items.length === 0 || !value) {
            return items;
        }
        console.log('Pipe: ', value, ' Field: ', field);
        console.dir(items);
        return items.filter((i) => {
            const t = Object.assign({}, i);

            if (!isNaN(t[field])) {
                t[field] += '';
            }

            if (field === 'category') {
                console.log('catName: ', t['catName'])
                t[field] = t['catName'];
            }

            if (field === 'type') {
                t[field] = t[field] === 'income' ? 'доход' : 'расход';
            }
            return t[field].toLowerCase().indexOf(value.toLowerCase()) !== -1;
        });
    }
}
