import { Component, OnInit, Input } from '@angular/core';
import { BMEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/categoty.model';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'bm-history-events',
  templateUrl: './history-events.component.html',
  styleUrls: ['./history-events.component.scss']
})
export class HistoryEventsComponent implements OnInit {

  @Input() categories: Category[];
  @Input() events: BMEvent[];
  searchPlaceholder = 'Сумма';
  searchField = 'amount';
  searchValue = '';

  constructor() { }

  getEventClass(e: BMEvent) {
    return {
      'label': true,
      'label-danger': e.type === 'outcome',
      'label-success': e.type === 'income'
    };
  }

  changeCriteria(field: string) {
    const namesMap = {
      amount: 'Сумма',
      date: 'Дата',
      category: 'Категория',
      type: 'Тип'
    };
    this.searchPlaceholder = namesMap[field];
    this.searchField = field;
  }

  ngOnInit() {
    this.events.forEach((e) => {
      e.catName = this.categories.find((c) => c.id === e.category).name;
    });
  }

}
