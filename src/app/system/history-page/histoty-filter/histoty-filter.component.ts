import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Category } from '../../shared/models/categoty.model';

@Component({
  selector: 'bm-histoty-filter',
  templateUrl: './histoty-filter.component.html',
  styleUrls: ['./histoty-filter.component.scss']
})
export class HistotyFilterComponent implements OnInit {

  @Output() FilterCancel = new EventEmitter();
  @Output() FilterApply = new EventEmitter();

  @Input() categories: Category[] = [];

  selectedPeriod = 'd';
  selectedType = [];
  selectedCategories = [];

  timePeriods = [
    { type: 'd', label: 'День' },
    { type: 'w', label: 'Неделя' },
    { type: 'M', label: 'Месяц' }
  ];

  types = [
    { type: 'income', label: 'Доход' },
    { type: 'outcome', label: 'Расход' }
  ];

  constructor() { }

  ngOnInit() {
  }

  closeFilter() {
    this.selectedCategories = [];
    this.selectedPeriod = 'd';
    this.selectedType = [];
    this.FilterCancel.emit();
  }

  private calculateInputParams(field: string, checked: boolean, value: string) {
    if (checked) {
      this[field].indexOf(value) === -1 ? this[field].push(value) : null;
    } else {
      this[field] = this[field].filter(i => i !== value);
    }
  }

  handleChangeType({checked, value}) {
    this.calculateInputParams('selectedType', checked, value);
  }

  handleChangeCategory({checked, value}) {
    this.calculateInputParams('selectedCategories', checked, value);
  }

  applyFilter() {
    this.FilterApply.emit({
      types: this.selectedType,
      categories: this.selectedCategories,
      period: this.selectedPeriod
    });
  }
}
