import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bm-currency-card',
  templateUrl: './currency-card.component.html',
  styleUrls: ['./currency-card.component.scss']
})
export class CurrencyCardComponent implements OnInit {

  @Input() currency: any;
  currencies: string[] = ['USD', 'EUR'];
  nowDate: Date = new Date();

  constructor() { }

  ngOnInit() {
  }

}
